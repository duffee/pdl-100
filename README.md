# 100 PDL (Perl Data Language) Exercises 

This is a perl port of 100 Numpy exercises (https://github.com/rougier/numpy-100)

Originally proposed here: https://perlmonks.org/?node_id=1213987 by mxb.

100 Numpy exercises is a collection of exercises that have been collected in the numpy mailing list, on stack overflow and in the numpy documentation. The goal of this collection is to offer a quick reference for both old and new users but also to provide a set of exercises for those who teach.

If you find an error or think you've a better way to solve some of them, feel free to open an issue.

TODO: 19
WIP: 1
N/A:  16

64/100 COMPLETED

#### 1. Import PDL (★☆☆)


```perl
use PDL;
```

#### 2. Print the PDL version and the configuration (★☆☆)


```perl
use PDL::Version;
print $PDL::Version::VERSION;
```

#### 3. Create a null vector of size 10 (★☆☆)


```perl
my $z = zeros(10);
print $z;
```

#### 4.  How to find the memory size of any array (★☆☆)


```perl
use PDL::Core ':Internal'
my $z = zeros(10);
my $size = howbig($z->get_datatype) * $z->nelem;
print $size;

# author : vr
print $z-> info( '%M' );
```

#### 5.  How to get the documentation of the PDL add function from the command line? (★☆☆)


```perl
# To get top level PDL help
perldoc PDL
```

#### 6.  Create a null vector of size 10 but the fifth value which is 1 (★☆☆)


```perl
my $z = zeros(10);
$z->slice(4) .= 1;
print $z;

or with PDL::NiceSlice loaded:

my $z = zeros(10);
$z(4) .= 1;
print $z;
```

#### 7.  Create a vector with values ranging from 10 to 49 (★☆☆)


```perl
my $z = 10 + sequence(40);
print $z;
```

#### 8.  Reverse a vector (first element becomes last) (★☆☆)


```perl
my $z = sequence(10);
$z = $z(-1:0);
print $z;
```

#### 9.  Create a 3x3 matrix with values ranging from 0 to 8 (★☆☆)


```perl
my $z = sequence(3,3);
print $z;
```

#### 10. Find indices of non-zero elements from \[1,2,0,0,4,0\] (★☆☆)


```perl
my $vec = pdl [1,2,0,0,4,0];
my $nz = $vec->which;
print $nz;
```

#### 11. Create a 3x3 identity matrix (★☆☆)


```perl
my $z = identity(3);
print $z;
```

#### 12. Create a 3x3x3 array with random values (★☆☆)


```perl
my $z = random(3,3,3);
print $z;
```

#### 13. Create a 10x10 array with random values and find the minimum and maximum values (★☆☆)


```perl
my $z = random(10,10);
print $z->min, $z->max;
```

#### 14. Create a random vector of size 30 and find the mean value (★☆☆)


```perl
my $z = random(30);
print $z->avg;
```

#### 15. Create a 2d array with 1 on the border and 0 inside (★☆☆)


```perl
my $z = ones(10,10);
$z(1:8,1:8) .= 0;
print $z;
```

#### 16. How to add a border (filled with 0's) around an existing array? (★☆☆)


```perl
my $a = random(8,8);
my $x = zeros(2 + $a->dim(0), 2 + $a->dim(1));
$x(1:8,1:8) .= $a;
print $x;
```

#### 17. What is the result of the following expression? (★☆☆)


```perl
n/a
```

#### 18. Create a 5x5 matrix with values 1,2,3,4 just below the diagonal (★☆☆)


```perl
my $z = identity(5) * (1 + sequence(5));
$z->where($z > 4) .= 0;
$z = $z->transpose->rotate(1)->transpose;
print $z;
```

#### 19. Create a 8x8 matrix and fill it with a checkerboard pattern (★☆☆)


```perl
my $z = zeros(8,8);
$z("0::2","0::2") .= 1
$z("1::2","1::2") .= 1
print $z;
```

#### 20. Consider a (6,7,8) shape array, what is the index (x,y,z) of the 100th element?


```perl
# dimensions seem to be handled in the reverse way to numpy
# therefore the input array needs to be declared backwards, and the results are backwards too
# i suspect this is actually the 101st element as it counts from zero, but this matches the
# result in the numpy exercises

my $a=random(8,7,6);
print one2nd($a, 100);
```

#### 21. Create a checkerboard 8x8 matrix using the tile function (★☆☆)


```perl
n/a
```

#### 22. Normalize a 5x5 random matrix (★☆☆)


```perl
my $z = random(5,5);
$z = (($z - $z->min) / ($z->max - $z->min));
print $z;
```

#### 23. Create a custom dtype that describes a color as four unsigned bytes (RGBA) (★☆☆)


```perl
n/a
```

#### 24. Multiply a 5x3 matrix by a 3x2 matrix (real matrix product) (★☆☆)


```perl
my $x = ones(3,5);
my $y = ones(2,3);
print $x x $y;
```

#### 25. Given a 1D array, negate all elements which are between 3 and 8, in place. (★☆☆)


```perl
my $z = sequence(10);
$z->where($z <= 8 & $z>= 3) *= -1;
print $z;
```

#### 26. What is the output of the following script? (★☆☆)


```perl
n/a
```

#### 27. Consider an integer vector Z, which of these expressions are legal? (★☆☆)


```perl
my $z = sequence(long, 10);
$z ** $z;
2 << $z >> 2;
$z <- $z;
1j * $z;
$z / 1 / 1;
$z < $z > $z;
```

#### 28. What are the result of the following expressions?


```perl
print pdl(0) / pdl(0);
print pdl(0) // pdl(0);
print float int pdl(NaN);
```

#### 29. How to round away from zero a float array ? (★☆☆)


```perl
$z = 20 * random(10) - 10;
$z->where($z < 0) .= - $z->where($z < 0)->abs->ceil;
$z->where($z > 0) .= $z->where($z > 0)->ceil;
print $z;
```

#### 30. How to find common values between two arrays? (★☆☆)


```perl
my $z1 = long 256 * random(10);
my $z2 = long 256 * random(10);
print intersect $z1, $z2;
```

#### 31. How to ignore all warnings (not recommended)? (★☆☆)


```perl
n/a for PDL
```


#### 32. Is the following expressions true? (★☆☆)


```perl
n/a
```

#### 33. How to get the dates of yesterday, today and tomorrow? (★☆☆)


```perl
# No built in PDL time/date functions
my $yesterday = time() - (60 * 60 * 24);
my $today = time();
my $tomorrow = time() + (60 * 60 * 24);
```

#### 34. How to get all the dates corresponding to the month of July 2016? (★★☆)


```perl
n/a
```

#### 35. How to compute ((A+B)\*(-A/2)) in place (without copy)? (★★☆)


```perl
my $a = ones(3);
my $b = 2 * ones(3);

$b += $a;
$a /= -2;
$b *= $a;

print $b;
```

#### 36. Extract the integer part of a random array using 5 different methods (★★☆)


```perl
my $z = 10 * random(10);
print $z->ceil;
print $z->floor;
print byte $z;
print long $z;
print longlong $z;
```

#### 37. Create a 5x5 matrix with row values ranging from 0 to 4 (★★☆)


```perl
my $z = xvals zeros(5,5);
print $z;
```

#### 38. Consider a generator function that generates 10 integers and use it to build an array (★☆☆)


```perl
n/a
```

#### 39. Create a vector of size 10 with values ranging from 0 to 1, both excluded (★★☆)


```perl
my $z = (sequence(12) / 11)->slice("1:10");
print $z;
```

#### 40. Create a random vector of size 10 and sort it (★★☆)


```perl
my $z = random(10)->qsort;
print $z;
```

#### 41. How to sum a small array faster than np.sum? (★★☆)


```perl
n/a
```

#### 42. Consider two random array A and B, check if they are equal (★★☆)


```perl
my $a = random(10);
my $b = random(10);
print $a == $b;
```

#### 43. Make an array immutable (read-only) (★★☆)


```perl
n/a
```

#### 44. Consider a random 10x2 matrix representing cartesian coordinates, convert them to polar coordinates (★★☆)


```perl
use PDL::Complex;
my $z = random(2,10);
my $p = Cr2p($z);
print $p;
```

#### 45. Create random vector of size 10 and replace the maximum value by 0 (★★☆)


```perl
# author: vr
my $z = random( 10 );
$z( $z-> maximum_ind ) .= 0;
print $z;

my $z = random(10);
$z->where($z == $z->max) .= 0;
print $z;
```

#### 46. Create a structured array with `x` and `y` coordinates covering the \[0,1\]x\[0,1\] area (★★☆)


```perl
n/a
```

####  47. Given two arrays, X and Y, construct the Cauchy matrix C (Cij =1/(xi - yj))


```perl
# author: Ea

use PDL;
use PDL::NiceSlice;

my $x = sequence(8);
my $y = $x + 0.5;

my ($nx, $ny) = (nelem($x), nelem($y));

my $C = zeroes($nx, $ny);

for (my $i = 0; $i < $nx; $i++) {
    for (my $j = 0; $j < $ny; $j++) {
        $C($i,$j) .= 1/($x($i) - $y($j));
    }
}

print $C;
```

#### 48. Print the minimum and maximum representable value for each scalar type (★★☆)


```perl
# This cannot be done directly, but you can extract the underlying
# C type used for each PDL type:

print byte->realctype;
print short->realctype;
print ushort->realctype;
print long->realctype;
print longlong->realctype;
print indx->realctype;
print float->realctype;
print double->realctype;
```

#### 49. How to print all the values of an array? (★★☆)


```perl
# Set maximum print limit to one million elements
$PDL::toolongtoprint = 1_000_000;
$z = zeros(1000,1000);
print $z;
```

#### 50. How to find the closest value (to a given scalar) in a vector? (★★☆)


```perl
my $Z = sequence(100);
my $v = random(1) * 100;
#print $Z;
print $v;
my $a = abs($Z - $v);
my $index = which($a == $a->min());
#print $a;
print $Z->at($index->at(0));
```

#### 51. Create a structured array representing a position (x,y) and a color (r,g,b) (★★☆)


```perl
n/a
```

#### 52. Consider a random vector with shape (100,2) representing coordinates, find point by point distances (★★☆)


```perl
my $z = random(10,2);
my ($x,$y) = ($z(:,0), $z(:,1));
my $d = (($x - $x->transpose)->ipow(2)) +
        (($y - $y->transpose)->ipow(2));
print $d;
```

#### 53. How to convert a float (32 bits) array into an integer (32 bits) in place?


```perl
my $z = float 1000 * random(10);
$z = long $z;
```

#### 54. How to read the following file? (★★☆)


```perl
my $z = rcols "data.csv", { COLSEP => ',' }, [];
$z = $z->transpose;     # optional (PDL is column major)
print $z;
```

#### 55. What is the equivalent of enumerate for numpy arrays? (★★☆)


```perl
n/a
```

#### 56. Generate a generic 2D Gaussian-like array (★★☆)


```perl
my $z = grandom(10,10);     # correct?
print $z;
```

#### 57. How to randomly place p elements in a 2D array? (★★☆)


```perl
my $p = 3;
my $z = zeros(10,10);
my $i = indx $z->nelem * random($p)
$z->clump($z->ndims)->($i) .= 1;
print $z;
```

#### 58. Subtract the mean of each row of a matrix (★★☆)


```perl
my $z = random(5, 10);
$z = $z - $z->avgover->transpose;
print $z;
```

#### 59. How to I sort an array by the nth column? (★★☆)


```perl
# not ideal as we have to leave pdl to do multidimensional sorting
my $n = 4;
my $pdl = random(5,5);
my $notpdl = unpdl($pdl);
my @notpdl = sort { $a->[$n] <=> $b->[$n] } @$notpdl;
print $pdl;
$pdl = pdl(\@notpdl);
print $pdl;
```

#### 60. How to tell if a given 2D array has null columns? (★★☆)


```perl
# WIP
# AFAICT, PDL has no 'null' values, only bad values and nan
# which are not quite the same thing
# we can configure bad values to be nan to make things easier
# and check as below, bit of a fudge
my $a = pdl(qw(bad bad 1234));
my $b = pdl(qw(nan nan 1234));
if ($b->uniq->at($b->uniq->nelem - 1) eq 'nan') {
    print '$b contains nan';
}
#if ($a->nbad > 0) {
#       print '$a contains bad';        
#}
```

#### 61. Find the nearest value from a given value in an array (★★☆)


```perl
TODO
```

#### 62. Considering two arrays with shape (1,3) and (3,1), how to compute their sum using an iterator? (★★☆)


```perl
#WIP, maybe use this : https://metacpan.org/pod/PDL::NDBin
my $a = sequence(1,3);
my $b = sequence(3,1);
my $c = zeroes(3,3);
$c->slice('0,') .= $a;
$c->slice(',0') .= $b;
print $c;
```

#### 63. Create an array class that has a name attribute (★★☆)


```perl
n/a
```

#### 64. Consider a given vector, how to add 1 to each element indexed by a second vector (be careful with repeated indices)? (★★★)


```perl
# author: vr

my $z = zeroes( 10 );
my $i = pdl( 1, 3, 5, 3, 1 );
indadd( 1, $i, $z );
print $z;
```

#### 65. How to accumulate elements of a vector (X) to an array (F) based on an index list (I)? (★★★)


```perl
TODO
```

#### 66. Considering a (w,h,3) image of (dtype=ubyte), compute the number of unique colors (★★★)


```perl
my ($w, $h) = (16, 16);
my $i = byte 256 * random($w, $h, 3);
my $uniqcol = $i->uniq->nelem;
print $uniqcol;
```

#### 67. Considering a four dimensions array, how to get sum over the last two axis at once? (★★★)


```perl
TODO
```

#### 68. Considering a one-dimensional vector D, how to compute means of subsets of D using a vector S of same size describing subset  indices? (★★★)


```perl
TODO
```

#### 69. How to get the diagonal of a dot product? (★★★)


```perl
my $z1 = random(10, 10);
my $z2 = random(10, 10);
print $z1->inner($z2);
```

#### 70. Consider the vector \[1, 2, 3, 4, 5\], how to build a new vector with 3 consecutive zeros interleaved between each value? (★★★)


```perl
my $z = pdl [1,2,3,4,5];
my $nz = 3;
my $x = zeros($z->dim(0) * $nz);
$x("0::$nz") .= $z;
print $x;
```

#### 71. Consider an array of dimension (5,5,3), how to mulitply it by an array with dimensions (5,5)? (★★★)


```perl
my $z1 = ones(5,5,3);
my $z2 = 2 * ones(5,5);
print $z1 * $z2;
```

#### 72. How to swap two rows of an array? (★★★)


```perl
my $z = sequence(5,5);
$z(0:1,) .= $z(1:0,)->sever;
print $z;
```

#### 73. Consider a set of 10 triplets describing 10 triangles (with shared vertices), find the set of unique line segments composing all the  triangles (★★★)


```perl
TODO
```

#### 74. Given an array C that is a bincount, how to produce an array A such that np.bincount(A) == C? (★★★)


```perl
TODO
```

#### 75. How to compute averages using a sliding window over an array? (★★★)


```perl
TODO
```

#### 76. Consider a one-dimensional array Z, build a two-dimensional array whose first row is (Z\[0\],Z\[1\],Z\[2\]) and each subsequent row is  shifted by 1 (last row should be (Z\[-3\],Z\[-2\],Z\[-1\]) (★★★)


```perl
TODO
```

#### 77. How to negate a boolean, or to change the sign of a float inplace? (★★★)


```perl
my $z = long 2 * random(10);
$z = not $z;
print $z;
$z = -5 + sequence(10);
$z = -1 * $z;
print $z;
```

#### 78. Consider 2 sets of points P0,P1 describing lines (2d) and a point p, how to compute distance from p to each line i  (P0\[i\],P1\[i\])? (★★★)


```perl
TODO
```

#### 79. Consider 2 sets of points P0,P1 describing lines (2d) and a set of points P, how to compute distance from each point j (P\[j\]) to each line i (P0\[i\],P1\[i\])? (★★★)


```perl
TODO
```

#### 80. Consider an arbitrary array, write a function that extract a subpart with a fixed shape and centered on a given element (pad with a `fill` value when necessary) (★★★)


```perl
TODO
```

#### 81. Consider an array Z = \[1,2,3,4,5,6,7,8,9,10,11,12,13,14\], how to generate an array R = \[\[1,2,3,4\], \[2,3,4,5\], \[3,4,5,6\], ..., \[11,12,13,14\]\]? (★★★)


```perl
#author: vr
my $z = 1 + sequence 14;
my $len = 4;

print $z-> lags( 0, 1, 1 + $z-> nelem - $len )
        -> slice( '','-1:0' );


# old solution
$z = 10 * random(15);
$len = 4;
my @r = ();
push @r,  $z($_:$_ + $len-1) for (0 .. $z->nelem - $len)
$r = pdl @r;
print $r;
```

#### 82. Compute a matrix rank (★★★)


```perl
my $z = 10 * random(10,10);
my ($u, $s, $v) = $z->svd;
my $rank = $s->where($s > 1e-10);
print $rank;
```

#### 83. How to find the most frequent value in an array?


```perl
TODO
```

#### 84. Extract all the contiguous 3x3 blocks from a random 10x10 matrix (★★★)


```perl
my $z = long 5 * random(10,10);
my $dim = 3;
my (@out, $out);
for my $i ( 0 .. $z->dim(0) - $dim - 1) {
    for my $j ( 0 .. $z->dim(1) - $dim - 1) {
        push @out, $z($i:$i+$dim,$j:$j+$dim);
    }
}
$out = pdl @out;
print $out;
```

#### 85. Create a 2D array subclass such that Z\[i,j\] == Z\[j,i\] (★★★)


```perl
TODO
```

#### 86. Consider a set of p matrices wich shape (n,n) and a set of p vectors with shape (n,1). How to compute the sum of of the p matrix products at once? (result has shape (n,1)) (★★★)


```perl
TODO
```

#### 87. Consider a 16x16 array, how to get the block-sum (block size is 4x4)? (★★★)


```perl
#author: vr, 2 solutions
my $x = sequence 16, 16;

print $x-> lags( 1, 4, 4 )
        -> slice( '', '', '-1:0' )
        -> xchg( 0, 1 )
        -> sumover
        -> lags( 0, 4, 4 )
        -> slice( '', '-1:0' )
        -> sumover;

my $x = sequence 16, 16;

print $x-> reshape( 4, 4, 4, 4 )
        -> reorder( 0, 2, 1, 3 )
        -> clump( 2 )
        -> sumover

```

#### 88. How to implement the Game of Life using numpy arrays? (★★★)


```perl
TODO
```

#### 89. How to get the n largest values of an array (★★★)


```perl
my $z = 10 * random(20);
my $n = 3;
print $z->qsort->(-$n:);
```

#### 90. Given an arbitrary number of vectors, build the cartesian product (every combinations of every item) (★★★)


```perl
TODO
```

#### 91. How to create a record array from a regular array? (★★★)


```perl
n/a
```

#### 92. Consider a large vector Z, compute Z to the power of 3 using 3 different methods (★★★)


```perl
my $z = random(5e7);
$z ** 3;
$z->ipow(3);
$z->power(3,0);
```

#### 93. Consider two arrays A and B of shape (8,3) and (2,2). How to find rows of A that contain elements of each row of B regardless of the order of the elements in B? (★★★)


```perl
use strict;
use warnings; 
use PDL;

my $A = floor(random(3,8) * 6);
my $B = floor(random(2,2) * 6);

my $C = $A->in($B->slice(',0'))->sumover * $A->in($B->slice(',1'))->sumover ;
my $rows = which($C > 0);
print $rows;
```

#### 94. Considering a 10x3 matrix, extract rows with unequal values (e.g. \[2,2,3\]) (★★★)


```perl
TODO
```

#### 95. Convert a vector of ints into a matrix binary representation (★★★)


```perl
my $z = pdl [0,1,2,3,15,16,32,64,128];
my $bits = ($z->transpose & (2 ** xvals(9)));
$bits->where($bits > 0) .= 1;
print $bits;
```

#### 96. Given a two dimensional array, how to extract unique rows? (★★★)


```perl
my $z = long 2 * random(3,6);
print $z->uniqvec;
```

#### 97. Considering 2 vectors A & B, write the einsum equivalent of inner, outer, sum, and mul function (★★★)


```perl
TODO
```

#### 98. Considering a path described by two vectors (X,Y), how to sample it using equidistant samples (★★★)?


```perl
TODO
```

#### 99. Given an integer n and a 2D array X, select from X the rows which can be interpreted as draws from a multinomial distribution with n degrees, i.e., the rows which only contain integers and which sum to n. (★★★)


```perl
# author: vr

use strict;
use warnings;
use PDL;

my $x = pdl([1.0, 0.0, 3.0, 8.0],
            [2.0, 0.0, 1.0, 1.0],
            [1.5, 2.5, 1.0, 0.0]);
my $n = 4;

my $mask = ( $x == $x-> rint )-> andover
            &
           ( $x-> sumover == $n );

print $x-> transpose
        -> whereND( $mask )
        -> transpose;
```

#### 100. Compute bootstrapped 95% confidence intervals for the mean of a 1D array X (i.e., resample the elements of an array with replacement N times, compute the mean of each sample, and then compute percentiles over the means). (★★★)


```perl
# author: vr

use strict;
use warnings;
use feature 'say';
use PDL;

my $n   = 100;          # input sample size
my $m   = 1000;         # number of bootstrap repeats
my $r   = $n;           # re-sample size

my $x   = random $n;
my $idx = random $r, $m;
$idx   *= $n;

say $x-> index( $idx )
      -> avgover
      -> pctover( pdl 0.05, 0.95 );

__END__

[ 0.4608755 0.55562806]
```
